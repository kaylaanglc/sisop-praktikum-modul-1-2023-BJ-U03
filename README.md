# Praktikum Sisop Modul 1 (BJ-U03)
Group Members:
1. Ahmad Danindra Nugroho (5025211259)
2. Mardhatillah Shevy Ananti (5025211070)
3. Raden Roro Kayla Angelica Priambudi (5025211262)

## Question #1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. 
Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja
Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi:

### 1A.
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu.
Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

#### Source Code:
```sh
echo ---1A. Top 5 Highest Ranked Universities in Japan---
cat qs_wur_2023.csv | grep "Japan" | sort -t, -k1n | head -5 | awk -F ',' '{print $1, $2, $4}'
```
#### Explanation:
The purpose of this command is to extract and print information from the ``qs_wur_2023.csv`` file, specifically the top 5 highest ranked universities in Japan for the year 2023.

Here is a breakdown of each part of the command:
- ``cat qs_wur_2023.csv``: This command reads the contents of the file ``qs_wur_2023.csv`` and outputs it to the standard output.
- ``grep "Japan"``: This command searches for lines in the output of the previous command that contain the string "Japan".
- ``sort -t, -k1n``: This command sorts the lines of output based on the first column (the university ranking), which is separated by a comma (hence the ``-t`` option), in numerical order (``-k1n``).
- ``head -5``: This command outputs the first 5 lines of the sorted output.
- ``awk -F ',' '{print $1, $2, $4}'``: This command processes the output of the previous command using the ``awk`` command. The ``-F ','`` option sets the field separator to a comma. The ``{print $1, $2, $4}`` command prints the 1st, 2nd, and 4th columns of the input (which are the university ranking, university name, and country, respectively), separated by a space.

### 1B.
Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari
Faculty Student Score(fsr score) yang paling rendah diantara 5
Universitas di Jepang.

#### Source Code:
```sh
echo ---1B. 5 Universities with the Lowest Faculty Student Score in Japan---
cat qs_wur_2023.csv | grep "Japan" | sort -t, -k9n | head -5 | awk -F ',' '{print $2, $9}'
```
#### Explanation:

- ``cat qs_wur_2023.csv``: This command displays the contents of the ``qs_wur_2023.csv`` file on the console.
- ``grep "Japan"``: This command filters out all the lines from the CSV file that do not contain the word "Japan" in them.
- ``sort -t, -k9n``: This command sorts the remaining lines in ascending order based on the 9th column of the CSV file (which contains the FSR Score). The ``-t``, flag specifies that the delimiter in the CSV file is a comma and the ``-k9n`` flag tells sort to sort numerically based on the 9th column.
- ``head -5``: This command takes the first five lines of the sorted output.
- ``awk -F ',' '{print $2, $9}'``: Finally, this command prints out only the second and ninth columns of the remaining lines. The ``-F ','`` flag specifies that the delimiter in the CSV file is a comma and ``{print $2, $9}`` tells awk to print only the 2nd and 9th columns separated by a space.

### 1C.
Karena Bocchi takut salah membaca ketika memasukkan nama
universitas, cari 10 Universitas di Jepang dengan Employment Outcome
Rank(ger rank) paling tinggi.

#### Source Code:
```sh
cat qs_wur_2023.csv | grep "Japan" | sort -t, -k20n | head -10 | awk -F ',' '{print $2, $20}'
```
#### Explanation:
- ``sort -t, -k20n``: This command sorts the remaining lines in ascending order based on the 20th column of the CSV file (which likely contains the Overall Score). The ``-t``, flag specifies that the delimiter in the CSV file is a comma and the ``-k20n`` flag tells sort to sort numerically based on the 20th column.
- ``head -10``: This command takes the first ten lines of the sorted output. Since the output is sorted in ascending order, these will be the ten universities with the lowest Overall Scores in Japan.
- ``awk -F ',' '{print $2, $20}’``: Finally, this command prints out only the second and twentieth columns of the remaining lines. The ``-F ‘,’`` flag specifies that the delimiter in the CSV file is a comma and ``{print $2, $20}`` tells awk to print only the 2nd and 20th columns separated by a space.

### 1D.
Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu
bocchi mencari universitas tersebut dengan kata kunci keren.

#### Source Code:
```sh
cat qs_wur_2023.csv | grep "Keren" | awk -F ',' '{print $2}'
```
#### Explanation:
- ``cat qs_wur_2023.csv``: prints the contents of the file ``"qs_wur_2023.csv"`` to standard output
- ``grep "Keren"``: searches for the string "Keren" in the output of the previous command
- ``awk -F ',' '{print $2}'``: separates the input lines into fields using ',' as the delimiter, and then prints the second field of each line.
Therefore, the output of this command will be the second field (column) of all the lines in the file ``"qs_wur_2023.csv"`` that contain the string "Keren".

### Output:
<img src="screenshots/output_no1.jpeg" width ="500">

## Question #2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut maka Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Kobeni membuat code yang dapat digunakan untuk mendownload gambar tentang Indonesia setiap 10 jam sekali, dengan jumlah download sesuai dengan jam saat ini. File-file yang didownload akan disimpan dalam folder dan file yang ditentukan, dan akan di-zip setiap 1 hari.

### 2A.
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst).

#### Source Code:
```sh
#!/bin/bash

hour=$(date +"%H")
num_photos=$hour
zip_urut=1

while true
do
    folder_num=$(ls -d kumpulan_* 2>/dev/null | wc -l)
    folder_name="kumpulan_$((folder_num+1)).FOLDER"
    mkdir $folder_name

    for ((i=1; i<=$num_photos; i++))
    do
        wget -qO "$folder_name/perjalanan_$i.jpg" "https://source.unsplash.com/800x600/?indonesia"
    done

    if [[ $(date +"%H") -eq 00 ]]
    
    ....//zipping code

    sleep 36000
done
```
#### Explanation:

- `hour=$(date +"%H")`: assigns the current hour to the variable hour by running the date command and formatting the output with the `+"%H"` option to show the hour in 24-hour format.
- `num_photos=$hour`: assigns the value of hour to the variable `num_photos`.
- `while true`: starts an infinite loop
- `folder_num =$(ls -d kumpulan_* 2>/dev/null | wc -l)`: runs the ls command with the -d option to list directories that start with `"kumpulan_"`, and pipes the output to wc -l to count the number of matching directories. The output is assigned to the variable folder_num. The `2>/dev/null` part redirects any error messages to `/dev/null`, which discards them.
- `folder_name`=`"kumpulan_$((folder_num+1)).FOLDER"`: creates a string that represents the name of the new directory to be created. The name is constructed by concatenating the string `"kumpulan_"` with the value of `folder_num+1` enclosed in double parentheses to perform arithmetic expansion and adding the string `".FOLDER"`. The result is assigned to the variable `folder_name`.
- `mkdir $folder_name`: creates a new directory with the name stored in `folder_name`.
- `for ((i=1; i<=$num_photos; i++))`: starts a loop that will run `num_photos` times.
- `wget -qO "$folder_name/perjalanan_$i.jpg`" "https://source.unsplash.com/800x600/?indonesia": downloads an image from the website "https://source.unsplash.com/800x600/?indonesia" using the `wget` command and saves it with a filename constructed by concatenating the value of `folder_name`, the string `"/perjalanan_"`, and the value of the loop variable i followed by the string ".jpg". The `-qO` option tells `wget` to run quietly and output the downloaded file to standard output, which is then redirected to the specified file.
- `sleep 36000`: waits for 36000 seconds before looping to make folder again.

#### Output:
<img src="screenshots/Output_pembuatan_folder_soal_nomer_2.png" width ="500"> <img src="screenshots/Output_foto_dalam_folder_soal_nomer_2.png" width ="200">

### 2B.
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

#### Source Code
```sh
#!/bin/bash

hour=$(date +"%H")
num_photos=$hour
zip_urut=1

if [[ $(date +"%H") -eq 00 ]]; then
        zip_name="devil_$zip_urut.zip"
        zip -r "$zip_name" kumpulan_*.FOLDER
        rm -rf kumpulan_*.FOLDER
        ((zip_urut++))
        num_photos=1
    else
        num_photos=0
    fi
```
#### Explanation:

- `zip_urut=1`: assigns the value of 1 to the variable `zip_urut`, which will be used later to name the zip file.
- `if [[ $(date +"%H") -eq 00 ]]`; then: checks if the current hour is equal to 00 (midnight).
- `zip_name ="devil_$zip_urut.zip"`: creates a variable `zip_name` with the name "devil_" followed by the value of `zip_urut` variable and the extension `".zip"`.
- `zip -r "$zip_name" kumpulan_*.FOLDER`: archives all folders in the current directory that start with `"kumpulan_"` and have the extension `".FOLDER"` into a zip - file with the name stored in the zip_name variable.
- `rm -rf kumpulan_*.FOLDER`: removes all directories in the current directory that start with `"kumpulan_"` and have the extension `".FOLDER"`.
- `((zip_urut++))`: increments the zip_urut variable by 1.
- `num_photos=1`: sets the num_photos variable to 1.
- `else`: if the current hour is not equal to 00.
- `num_photos=0`: sets the num_photos variable to 0.

#### Output:
<img src="screenshots/Output_zip_soal_nomer_2.png" width ="500">

## Question #3
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

### 3A.
Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username 
- Tidak boleh menggunakan kata chicken atau ernie

#### Source Code:
```sh
#!/bin/bash

mkdir -p "users"

users_list="users/users.txt"

function log {
echo "$(date +'%Y/%m/%d %H:%M:%S') $1: $2 $3" >> users/log.txt
}

function main {

echo "Registration System"

echo "Enter username:"
read username

echo "Enter password:"
read password

if [[ "$(grep "$username" "$users_list")" ]]; then
echo -e "\nUsername already exists, please try other username"
log "ERROR" "User already exists"

elif [[ "${#password}" -lt 8 ]]; then
echo -e "\nPassword should be a minimum of 8 characters"

elif [[ ! $(echo "$password" | grep '[a-z]' | grep '[A-Z]') ]]; then
echo -e "\nPassword must have atleast 1 capital letter and 1 lowercase letter"
   
elif [[ "$password" =~ [^a-zA-Z0-9] ]]; then
echo -e "\nPassword must use alphanumeric"

elif [[ "$password" == "$username" ]]; then
echo -e "\nPassword must not be the same as username"

elif [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]]; then
echo -e "\nPassword cannot use the words 'chicken' or 'ernie'"

else
echo "$username:$password" >> "$users_list"
echo -e "\nUsername successfully created"
log "INFO" "User $username registered succesfully"

fi
}

main

```
#### Explanation:
The purpose of this file is to make a simple registration system.

Here is a breakdown of each part of the command:
 - ``mkdir -p "users"``': This line creates a directory called "users" if it does not already exist. The -p flag tells mkdir to create the directory and any necessary parent directories.
 - ``users_list="users/users.txt"``: This line creates a variable called users_list and sets its value to "users/users.txt", which is the file where the registered users will be stored.
 - ``function log { ... }``: This line defines a function called log, which is used to log information to a file called "log.txt" in the "users" directory. 
 - ``function main { ... }``: This line defines a function called main, which contains the main logic of the registration system. It prompts the user for a username and password, checks if the username already exists, and checks if the password meets various requirements. If the username and password are valid, it adds the username and password to the users list file and logs the registration.
 - ``if [[ "$(grep "$username" "$users_list")" ]]``: This line checks if the username already exists in the users list file. If it does, it prints an error message and logs the error.
 - ``elif [[ "${#password}" -lt 8 ]]``: This line checks if the password is at least 8 characters long. If it is not, it prints an error message.
 - ``elif [[ ! $(echo "$password" | grep '[a-z]' | grep '[A-Z]') ]]``: This line checks if the password has at least one lowercase letter and at least one uppercase letter. If it does not, it prints an error message.
 - ``elif [[ "$password" =~ [^a-zA-Z0-9] ]]``: This line checks if the password contains only alphanumeric characters. If it contains non-alphanumeric characters, it prints an error message.
 - ``elif [[ "$password" == "$username" ]]``: This line checks if the password is the same as the username. If it is, it prints an error message.
 - ``elif [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]]``: This line checks if the password contains the words "chicken" or "ernie". If it does, it prints an error message.
 -``else``: This line is executed if none of the previous conditions are true. It adds the username and password to the users list file, prints a success message, and logs the registration.
 - ``echo "$username:$password" >> "$users_list"``: This line appends the username and password to the users list file.
 - ``log "INFO" "User $username registered succesfully"``: This line logs the registration to the log file with a log level of "INFO".

### Output:

<img src="screenshots/Soal3_input.jpeg" width ="500"> <img src="screenshots/Soal3_logfile.jpeg" width ="500">

### 3B.
Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

#### Source Code:
```sh
#!/bin/bash

users_list="users/users.txt"

function log {
echo "$(date +'%Y/%m/%d %H:%M:%S') $1: $2 $3" >> users/log.txt
}

function main {
echo "Login System"

echo "Enter Username:"
read username

echo "Enter Password:"
read password

if [[ ! "$(grep "$username" "$users_list")" ]] ; then
echo -e "\nUsername or password is wrong"
exit 1
fi

if [[ "$(grep "$username" "$users_list" | cut -d ':' -f 2)" == "$password" ]] >
echo -e "\nLogin successful"
log "INFO" "User $username logged in"

else 
echo -e "\nLogin failed"
log "ERROR" "Failed attempt on user $username"
exit 1
fi

}

main

```
#### Explanation:
The purpose of this file is to make a login system that checks the username and password entered by the user against the information stored in a file "users.txt" located in a directory "users/".

Here is a breakdown of each part of the command:
- ``function log { ... }``: This defines a function named log that takes three arguments and appends a log message to a file log.txt in the users directory.
- ``function main { ... }``: This defines a function named main that prompts the user to enter a username and password, checks if the username and password are valid by searching for them in the users.txt file, and logs the login attempt.
- ``if [[ ! "$(grep "$username" "$users_list")" ]] ; then ... fi``: This checks if the username entered by the user exists in the users.txt file. If not, it prints a message indicating that the username or password is wrong and exits the script.
- ``if [[ "$(grep "$username" "$users_list" | cut -d ':' -f 2)" == "$password" ]] > ... fi``: This checks if the password entered by the user matches the password associated with the username in the users.txt file. If so, it prints a message indicating that the login was successful and logs the event using the log function. If not, it prints a message indicating that the login failed, logs the event using the log function, and exits the script.

### Output:

<img src="screenshots/Soal3_input.jpeg" width ="500"> <img src="screenshots/Soal3_usersfile.jpeg" width ="500">

## Question #4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia
mengharuskan dirinya untuk mencatat log system komputernya.

### 4A.
File syslog tersebut harus memiliki ketentuan:
- Backup file log system dengan format jam:menit tanggal:bulan:tahun
(dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan
dengan jam dilakukannya backup seperti berikut:
    - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf
b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan
pukul 12, sehingga huruf b diganti dengan huruf alfabet yang
memiliki urutan ke 12+2 = 14
    - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke
empat belas, dan seterusnya.
    - Setelah huruf z akan kembali ke huruf a
- Backup file syslog setiap 2 jam untuk dikumpulkan.
#### Source Code:
```sh
#!/bin/bash

mkdir -p "encrypted_syslog"

lowerc="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
upperc="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"

hour=$(date "+%H")
date=$(date "+%H:%M %d:%m:%Y")

backup_file="/home/kaylaangelica/Desktop/prak1/encrypted_syslog/${date}.txt"

cat /var/log/syslog | tr "${lowercase:0:26}${uppercase:0:26}" "${lowercase:${hour}:26}${uppercase:${hour}:26}" > "${backup_file}"

# Cronjob command to automatically run every 2 hours:
# 0 */2 * * * /bin/bash /home/kaylaangelica/Desktop/prak1/log_encrypt.sh
```
#### Explanation:
This script creates a directory ``"encrypted_syslog"`` in the current directory if it does not exist, then encrypts the contents of the ``"/var/log/syslog"`` file and saves it to a new file in the ``"encrypted_syslog"`` directory. It also saves a backup of the encrypted file in the format of ``"HH:MM dd:mm:YYYY.txt"`` (hour, minute, day, month, year) in the same directory.

Here's a breakdown of the script:

- ``mkdir -p "encrypted_syslog"``: creates a directory named ``"encrypted_syslog"`` if it does not exist in the current directory.
- ``lowerc="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"`` and ``upperc="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"``: sets two variables ``"lowerc"`` and ``"upperc"`` to strings of lowercase and uppercase letters repeated twice to handle rotation over the hour change.
- ``hour=$(date "+%H")``: sets the variable ``"hour"`` to the current hour in 24-hour format.
- ``date=$(date "+%H:%M %d:%m:%Y")``: sets the variable ``"date"`` to the current time and date in the format of "HH:MM dd:mm:YYYY".
- ``backup_file="/home/kaylaangelica/Desktop/prak1/encrypted_syslog/${date}.txt"``: sets the variable ``"backup_file"`` to a backup file path in the ``"encrypted_syslog"`` directory with the filename as the current time and date.
- ``cat /var/log/syslog``: prints the contents of the ``"/var/log/syslog"`` file to standard output.
- ``tr "${lowercase:0:26}${uppercase:0:26}" "${lowercase:${hour}:26}${uppercase:${hour}:26}"``: applies the ``"tr"`` command to the input, which replaces each character of the lowercase and uppercase letters with the character ``"hour"`` positions to the right in the corresponding ``"lowerc"`` and ``"upperc"`` variables.
- ``> "${backup_file}"``: saves the output of the previous command to a new file named after the ``"backup_file"`` variable.

The script also includes a cronjob command at the end, which specifies that the script should run every two hours. This command should be added to the crontab file using the "crontab -e" command.

### 4B:
Buat juga script untuk dekripsinya.

#### Source Code:
```sh
#!/bin/bash

mkdir -p "decrypted_syslog"

read -p "Insert encrypted syslog file name: " enc_file

lowercase="abcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

file_hour=$(echo "$enc_file" | cut -d ':' -f1)

enc_path="/home/kaylaangelica/Desktop/prak1/encrypted_syslog/${enc_file}"
dec_path="/home/kaylaangelica/Desktop/prak1/decrypted_syslog/${enc_file}"

if [ -f "$enc_path" ] ; then
cat "${enc_path}" | tr "${lowercase:${file_hour}:26}${uppercase:${file_hour}:26}" "${lowercase:0:26}${uppercase:0:26}" > "${dec_path}"
fi
```
#### Explanation:
This script creates a directory named ``"decrypted_syslog"`` in the current directory if it does not exist, then prompts the user to input an encrypted syslog file name. It then decrypts the contents of the specified file and saves it to a new file in the ``"decrypted_syslog"`` directory.

Here's a breakdown of the script:
- ``mkdir -p "decrypted_syslog"``: creates a directory named ``"decrypted_syslog"`` if it does not exist in the current directory.
- ``read -p "Insert encrypted syslog file name: " enc_file``: prompts the user to input an encrypted syslog file name and sets the variable ``"enc_file"`` to the user input.
- ``lowercase="abcdefghijklmnopqrstuvwxyz"`` and ``uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZ"``: sets two variables ``"lowercase"`` and ``"uppercase"`` to strings of lowercase and uppercase letters.
- ``file_hour=$(echo "$enc_file" | cut -d ':' -f1)``: extracts the hour from the encrypted file name by using the ``"cut"`` command to split the file name by ``":"`` and taking the first field.
- ``enc_path="/home/kaylaangelica/Desktop/prak1/encrypted_syslog/${enc_file}"`` and ``dec_path="/home/kaylaangelica/Desktop/prak1/decrypted_syslog/${enc_file}"``: set the paths for the encrypted and decrypted files based on the user input file name.
- ``if [ -f "$enc_path" ] ; then``: checks if the encrypted file exists.
- ``cat "${enc_path}" | tr "${lowercase:${file_hour}:26}${uppercase:${file_hour}:26}" "${lowercase:0:26}${uppercase:0:26}" > "${dec_path}"``: decrypts the encrypted file by applying the ``"tr"`` command to the input, which replaces each character of the lowercase and uppercase letters with the character ``"hour"`` positions to the left in the corresponding ``"lowercase"`` and ``"uppercase"`` variables. The decrypted output is then saved to a new file named after the ``"dec_path"`` variable.

The script is designed to work with files that have been encrypted using the previous script. The encrypted files are expected to have a filename in the format of "HH:MM dd:mm:YYYY.txt", where "HH" is the hour of the encryption.

### Output:
- Encrypt:

<img src="screenshots/output_encrypt.jpeg" width ="500"> <img src="screenshots/isi_encrypt.jpeg" width ="500">
- Decrypt:

<img src="screenshots/output_decrypt.jpeg" width ="500"> <img src="screenshots/isi_decrypt.jpeg" width ="500">
