#!/bin/bash
echo ---1A. Top 5 Highest Ranked Universities in Japan---
cat qs_wur_2023.csv | grep "Japan" | sort -t, -k1n | head -5 | awk -F ',' '{print $1, $2, $4}'
echo
echo ---1B. 5 Universities with the Lowest Faculty Student Score in Japan---
cat qs_wur_2023.csv | grep "Japan" | sort -t, -k9n | head -5 | awk -F ',' '{print $2, $9}'
echo
echo ---1C. Top 10 Universities in Japan with the Highest Employment Outcome Rank---
cat qs_wur_2023.csv | grep "Japan" | sort -t, -k20n | head -10 | awk -F ',' '{print $2, $20}'
echo
echo ---1D. University with the Keyword: Keren---
cat qs_wur_2023.csv | grep "Keren" | awk -F ',' '{print $2}'