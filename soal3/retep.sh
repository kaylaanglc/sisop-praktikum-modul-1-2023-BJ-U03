#!/bin/bash
# login retep.sh

users_list="users/users.txt"

function log {
echo "$(date +'%Y/%m/%d %H:%M:%S') $1: $2 $3" >> users/log.txt
}

function main {
echo "Login System"

echo "Enter Username:"
read username

echo "Enter Password:"
read password

# check if username is registered
if [[ ! "$(grep "$username" "$users_list")" ]] ; then
echo -e "\nUsername or password is wrong"
exit 1
fi
# check if password is the same
if [[ "$(grep "$username" "$users_list" | cut -d ':' -f 2)" == "$password" ]] >
echo -e "\nLogin successful"
log "INFO" "User $username logged in"

else 
echo -e "\nLogin failed"
log "ERROR" "Failed attempt on user $username"
exit 1
fi

}

main
