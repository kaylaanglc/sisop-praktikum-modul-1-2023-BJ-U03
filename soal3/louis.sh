#!/bin/bash
# register program

# make users directory
mkdir -p "users"

users_list="users/users.txt"

function log {
echo "$(date +'%Y/%m/%d %H:%M:%S') $1: $2 $3" >> users/log.txt
}

function main {

echo "Registration System"

echo "Enter username:"
read username

echo "Enter password:"
read password

# check whether username is already registered in users.txt file
if [[ "$(grep "$username" "$users_list")" ]]; then
echo -e "\nUsername already exists, please try other username"
log "ERROR" "User already exists"

# check whether password has a minimum of 8 characters
elif [[ "${#password}" -lt 8 ]]; then
echo -e "\nPassword should be a minimum of 8 characters"

# check whether password password contains 1 capital letter and 1 lowercase
elif [[ ! $(echo "$password" | grep '[a-z]' | grep '[A-Z]') ]]; then
echo -e "\nPassword must have atleast 1 capital letter and 1 lowercase letter"
   
# check whether passwprd is alphanumeric
elif [[ "$password" =~ [^a-zA-Z0-9] ]]; then
echo -e "\nPassword must use alphanumeric"

# check whether the username and password must is the same 
elif [[ "$password" == "$username" ]]; then
echo -e "\nPassword must not be the same as username"

# check whether the password contains 'chicken' or 'ernie' 
elif [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]]; then
echo -e "\nPassword cannot use the words 'chicken' or 'ernie'"

else
echo "$username:$password" >> "$users_list"
echo -e "\nUsername successfully created"
log "INFO" "User $username registered succesfully"

fi
}

main
