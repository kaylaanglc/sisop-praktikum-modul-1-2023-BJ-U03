#!/bin/bash

hour=$(date +"%H")
num_photos=$hour
zip_urut=1

while true
do
    folder_num=$(ls -d kumpulan_* 2>/dev/null | wc -l)
    folder_name="kumpulan_$((folder_num+1)).FOLDER"
    mkdir $folder_name

    for ((i=1; i<=$num_photos; i++))
    do
        wget -qO "$folder_name/perjalanan_$i.jpg" "https://source.unsplash.com/800x600/?indonesia"
    done

    if [[ $(date +"%H") -eq 00 ]]; then
        zip_name="devil_$zip_urut.zip"
        zip -r "$zip_name" kumpulan_*.FOLDER
        rm -rf kumpulan_*.FOLDER
        ((zip_urut++))
        num_photos=1
    else
        num_photos=0
    fi

    sleep 36000
done
