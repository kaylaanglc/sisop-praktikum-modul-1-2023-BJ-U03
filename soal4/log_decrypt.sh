#!/bin/bash

mkdir -p "decrypted_syslog"

read -p "Insert encrypted syslog file name: " enc_file

lowercase="abcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

file_hour=$(echo "$enc_file" | cut -d ':' -f1)

enc_path="/home/kaylaangelica/Desktop/prak1/encrypted_syslog/${enc_file}"
dec_path="/home/kaylaangelica/Desktop/prak1/decrypted_syslog/${enc_file}"

if [ -f "$enc_path" ] ; then
cat "${enc_path}" | tr "${lowercase:${file_hour}:26}${uppercase:${file_hour}:26}" "${lowercase:0:26}${uppercase:0:26}" > "${dec_path}"
fi