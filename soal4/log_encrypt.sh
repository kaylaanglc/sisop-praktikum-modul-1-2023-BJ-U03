#!/bin/bash

mkdir -p "encrypted_syslog"

lowerc="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
upperc="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"

hour=$(date "+%H")
date=$(date "+%H:%M %d:%m:%Y")

backup_file="/home/kaylaangelica/Desktop/prak1/encrypted_syslog/${date}.txt"

cat /var/log/syslog | tr "${lowercase:0:26}${uppercase:0:26}" "${lowercase:${hour}:26}${uppercase:${hour}:26}" > "${backup_file}"

# Cronjob command to automatically run every 2 hours:
# 0 */2 * * * /bin/bash /home/kaylaangelica/Desktop/prak1/log_encrypt.sh